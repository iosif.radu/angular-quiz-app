import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import Config from "../config";

@Injectable({
  providedIn: "root"
})
export class TokenService {
  constructor(private http: HttpClient) {}

  requestToken(): Observable<any> {
    return this.http.get(Config.API_REQUEST_TOKEN_URL);
  }
}
