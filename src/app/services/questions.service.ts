import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import Config from "../config";

@Injectable({
  providedIn: "root"
})
export class QuestionsService {
  constructor(private http: HttpClient) {}

  requestQuestions(
    categoryId: number,
    amount: number,
    difficulty: string,
    type: string,
    token: string
  ): Observable<any> {
    return this.http.get(
      `${
        Config.API_QUESTIONS_URL
      }category=${categoryId}&amount=${amount}&difficulty=${difficulty}&type=${type}&token=${token}`
    );
  }
}
