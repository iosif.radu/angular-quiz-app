import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-result",
  templateUrl: "./result.component.html",
  styleUrls: ["./result.component.scss"]
})
export class ResultComponent {
  @Input() result: object;
  showAnswers = false;

  constructor() {}

  handleShowAnswers() {
    this.showAnswers = true;
  }
}
