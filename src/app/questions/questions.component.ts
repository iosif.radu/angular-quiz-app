import { Component, Input, Output, EventEmitter } from "@angular/core";
import { catchError } from "rxjs/operators";
import { of } from "rxjs";

import { QuestionsService } from "../services/questions.service";
import { Question } from "./question";

@Component({
  selector: "app-questions",
  templateUrl: "./questions.component.html",
  styleUrls: ["./questions.component.scss"]
})
export class QuestionsComponent {
  @Input() categoryId: number;
  @Input() amount: number;
  @Input() difficulty: string;
  @Input() type: string;
  @Input() generateNewQuestions: boolean;
  @Input() token: string;
  @Output() getTokenEvent = new EventEmitter();
  @Output() handleResultEvent = new EventEmitter();

  questions: Question[];
  currentQuestion: Question;
  currentQuestionNumber = 0;
  userScore = 0;
  numberOfQuestions: number;
  userAnswers: string[] = [];
  result = [];

  constructor(private questionsService: QuestionsService) {}

  ngOnChanges() {
    this.getQuestions();
  }

  getQuestions() {
    if (!this.token) {
      return this.getTokenEvent.emit();
    }
    const { amount, type, difficulty, token, categoryId } = this;
    this.questionsService
      .requestQuestions(categoryId, amount, difficulty, type, token)
      .pipe(
        catchError(err => {
          console.log(`Error while getting the questions by categoryId ${err}`);
          return of([]);
        })
      )
      .subscribe(res => {
        switch (res.response_code) {
          case 0:
            this.questions = res.results;
            this.numberOfQuestions = res.results.length;
            this.currentQuestion = this.questions[this.currentQuestionNumber];
            this.generateNewQuestions = false;
            break;
          case 1:
            console.log("Not enough result for your search");
            break;
          case 2:
            console.log("Invalid parameters getQuestions");
            break;
          case 3:
            console.log("Token Not Found Session Token does not exist.");
            this.getTokenEvent.emit();
            break;
          case 4:
            this.getTokenEvent.emit();
            console.log(
              "Token Empty Session Token has returned all possible questions for the specified query. Resetting the Token is necessary."
            );
            break;
          default:
            console.log("unknown status");
        }
      });
  }

  submitedAnswer(answer: string) {
    const { currentQuestion: question } = this;
    this.result.push({ question, answer });
    this.userAnswers.push(answer);
    if (
      answer.trim().localeCompare(this.currentQuestion.correct_answer) === 0
    ) {
      this.userScore += 1;
    }

    this.currentQuestionNumber += 1;
    if (this.numberOfQuestions === this.currentQuestionNumber) {
      const {
        userAnswers: answers,
        userScore: score,
        questions,
        result: results
      } = this;
      this.handleResultEvent.emit({ answers, score, questions, results });
    }
    this.currentQuestion = this.questions[this.currentQuestionNumber];
  }
}
