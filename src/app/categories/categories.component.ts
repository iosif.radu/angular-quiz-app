import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Category } from "./category";

@Component({
  selector: "app-categories",
  templateUrl: "./categories.component.html",
  styleUrls: ["./categories.component.scss"]
})
export class CategoriesComponent {
  @Input() categories: Category[];
  @Input() errorMessage: string;
  @Output() changeCategoryEvent = new EventEmitter();
  @Input() selectedCategory: Category;

  constructor() {}

  handleCategoryChange(category: Category) {
    event.preventDefault();
    this.changeCategoryEvent.emit(category);
  }
}
