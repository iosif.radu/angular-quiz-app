import { BrowserModule } from "@angular/platform-browser";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { MDBBootstrapModule, DropdownModule } from "angular-bootstrap-md";
import { CommonModule } from "@angular/common";

import { AppComponent } from "./app.component";
import { CategoriesComponent } from "./categories/categories.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { QuestionsComponent } from "./questions/questions.component";
import { QuestionComponent } from "./question/question.component";
import { ResultComponent } from "./result/result.component";

@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    DashboardComponent,
    QuestionsComponent,
    QuestionComponent,
    ResultComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    MDBBootstrapModule.forRoot(),
    DropdownModule.forRoot()
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
