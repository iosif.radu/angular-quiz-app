import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Question } from "../questions/question";
import * as he from "he";

@Component({
  selector: "app-question",
  templateUrl: "./question.component.html",
  styleUrls: ["./question.component.scss"]
})
export class QuestionComponent implements OnInit {
  @Input() question: Question;
  @Output() submitAnswerEvent = new EventEmitter();
  answers: string[];

  constructor() {}

  ngOnInit() {
    this.formatQuestion();
  }

  ngOnChanges() {
    this.formatQuestion();
  }

  handleClick(answer: string) {
    this.submitAnswerEvent.emit(answer);
  }

  formatQuestion() {
    this.question.question = he.decode(this.question.question);
    this.question.correct_answer = he.decode(this.question.correct_answer);
    this.question.incorrect_answers = this.question.incorrect_answers.map(
      iAnswer => he.decode(iAnswer)
    );

    this.shuffleAnswers(
      this.question.incorrect_answers,
      this.question.correct_answer
    );
  }

  shuffleAnswers(invalidAnswers: string[], correntAnswer: string) {
    this.answers = [...invalidAnswers];
    this.answers.splice(
      Math.floor(Math.random() * invalidAnswers.length + 1),
      0,
      correntAnswer
    );
  }
}
