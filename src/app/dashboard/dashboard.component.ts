import { Component, OnInit } from "@angular/core";

import { Category } from "../categories/category";
import { CategoryService } from "../services/category.service";
import { catchError } from "rxjs/operators";
import { of } from "rxjs";
import Config from "../config";
import { TokenService } from "../services/token.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  selectedCategory: Category;
  errorMessage: string;
  categories: Category[];
  result: object;
  finished = false;
  started = false;
  amount = Config.DEFAULT_AMOUT;
  difficulty = Config.MEDIUM_DIFFICULTY;
  type = Config.DEFAULT_TYPE;
  generateNewQuestions: boolean;
  token: string;

  constructor(
    private categoryService: CategoryService,
    private tokenService: TokenService
  ) {}

  ngOnInit() {
    this.getCategories();
    this.getToken();
  }

  getToken() {
    this.tokenService
      .requestToken()
      .pipe(
        catchError(err => {
          console.log(`Error while requesting a token ${err}`);
          return of([]);
        })
      )
      .subscribe(res => {
        this.token = res.token;
      });
  }

  getCategories() {
    this.categoryService
      .getCategories()
      .pipe(
        catchError(err => {
          this.errorMessage = "Error while trying to get the categories.";
          console.log("error in getCategories", err);
          return of([]);
        })
      )
      .subscribe(categories => {
        console.log("fetching categories.");
        this.categories = categories.trivia_categories;
        this.selectedCategory = categories.trivia_categories[0];
      });
  }

  changeCategory(category: Category) {
    this.selectedCategory = category;
  }

  selectNumberOfQuestions(value: number) {
    this.amount = value;
  }

  selectDifficulty(value: string) {
    this.difficulty = value;
  }

  selectQuestionsType(value: string) {
    this.type = value;
  }

  handleResult(result: object) {
    this.started = false;
    this.result = result;
    this.finished = true;
  }

  generateTest() {
    this.started = true;
    this.result = null;
  }

  changeSettings() {
    this.started = false;
    this.result = null;
  }
}
