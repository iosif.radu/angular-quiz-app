class Config {
  static API_CATEGORIES_URL = "https://opentdb.com/api_category.php";
  static API_QUESTIONS_URL = "https://opentdb.com/api.php?";
  static DEFAULT_AMOUT = 5;
  static EASY_DIFFICULTY = "easy";
  static MEDIUM_DIFFICULTY = "medium";
  static HARD_DIFFICULTY = "hard";
  static API_REQUEST_TOKEN_URL =
    "https://opentdb.com/api_token.php?command=request";
  static DEFAULT_TYPE = "multiple";
}

export default Config;
